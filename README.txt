safe README
==================

Getting Started
---------------

- cd <directory containing this file>

- $VENV/bin/python setup.py develop

- $VENV/bin/initialize_safe_db development.ini

- $VENV/bin/pserve development.ini



Supports only two operations as of now:

PUT
---

curl -v -XPUT http://0.0.0.0:6543/put?key=safe.url&value=localhost

GET
---

curl -v http://0.0.0.0:6543/get?key=safe.url


Dependencies:
-------------

- Pyramid
- Postgresql(psycopg2)
- Alembic(for migrations)
- Chameleon
