from pyramid.response import Response
from pyramid.view import view_config

from sqlalchemy.exc import DBAPIError

from .models import (
    DBSession,
    Config,
    )

from pyramid.httpexceptions import HTTPNotFound


@view_config(route_name='get', renderer="json")
def get_node_value(request):
    try:
        parent_path, key_name = request.params.get('key').rsplit('.', 1)
    except ValueError:
        key_name = request.params.get('key')
        parent_path = None
    config = DBSession.query(Config).filter_by(key = key_name, parent_path = parent_path).first()
    if config:
        return config.value
    else:
        request.response.status = '404 Not Found'
        return {"message" : "key not found" }

@view_config(request_method = 'PUT', route_name='put', renderer='json')
def put_node_value(request):
    try:
        parent_path, key_name = request.params.get('key').rsplit('.', 1)
    except ValueError:
        key_name = request.params.get('key')
        parent_path = None
    config_query = DBSession.query(Config).filter_by(key = key_name, parent_path = parent_path)
    if(config_query.first()):
        config_query.update({'value' : request.params.get('value')})
        config = config_query.first()
    else:
        config = Config(parent_path, key_name, request.params.get('value'))
        DBSession.add(config)
    return config.value

