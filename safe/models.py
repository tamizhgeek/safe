from sqlalchemy import (
    Column,
    Index,
    Integer,
    Text,
    )

from sqlalchemy.ext.declarative import declarative_base

from sqlalchemy.orm import (
    scoped_session,
    sessionmaker,
    )

from sqlalchemy.dialects.postgresql import JSON

from zope.sqlalchemy import ZopeTransactionExtension

DBSession = scoped_session(sessionmaker(extension=ZopeTransactionExtension()))
Base = declarative_base()


class Config(Base):
    __tablename__ = 'configs'
    id = Column(Integer, primary_key = True)
    parent_path = Column(Text)
    key = Column(Text)
    value = Column(JSON)

    
    def __init__(self, parent_path, key, value):
        self.parent_path = parent_path
        self.key = key
        self.value = value
