"""Create config table

Revision ID: 1fe4857f538
Revises: 
Create Date: 2014-12-27 13:16:36.172354

"""

# revision identifiers, used by Alembic.
revision = '1fe4857f538'
down_revision = None
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa

from sqlalchemy.dialects.postgresql import JSON

def upgrade():
    op.create_table(
        'configs',
        sa.Column('id', sa.Integer, primary_key = True),
        sa.Column('parent_path', sa.Text),
        sa.Column('key', sa.Text),
        sa.Column('value', JSON),
    )


def downgrade():
    op.drop_table('configs')
